# munin-snap

![](https://img.shields.io/badge/written%20in-PHP%2C%20Bash%2C%20expect-blue)

Munin scripts for retrieving information from the NZ ISP Snap Internet.

Also includes munin scripts for retrieving information (via telnet/expect) from the AVM Fritzbox router provided by Snap NZ.

Tags: scraper


## Download

- [⬇️ snapstats-r1.zip](dist-archive/snapstats-r1.zip) *(6.10 KiB)*
